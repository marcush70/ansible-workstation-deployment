# Setup workstation

## Description

Today my files, my browser setting, my emails, my password are backup in the cloud and easy to recover or reinstall on a new workstation. On the other hand, the configuration of my workstation was manual et and I never remember what was my previous/current setup.

Time to change this, the project allows me to rebuild a freshly new workstation in a matter of a minutes.

The project was inspired by this [blog post](https://opensource.com/article/18/3/manage-workstation-ansible).

The target is to use Ansible to configure my workstation, Azure key vault to store the password, ssh key needed, also see if my workstation is update compliance and slack to get a notification when ansible run with success or failure or if a reboot is pending.

## Roles

The following role will be applied on the workstation

1. __ws-azure-config__:

   Install needed package to retrieve passwords from Azure key vault and read the GitHub account of the OMS Linux agent.

2. __ws-ansible-config__:

    Install ansible account and cron job to retrieve any new configuration from the repo. Ansible will be trigger only if the repo is updated. The check is performed every 10 min.

3. __ws-facts-config__:

   Retreive the user home folder and name. I didn't want to put the full name of my user session publicly in the repo, so I used the session uid to retrieve dynamically the user name and build my home folder directory.

4. __ws-softwares-setup__:

    Installing all my software.

5. __ws-bitlocker-mount__:

   Coming from the Windows world, all my files are located on an SD card encrypted with BitLocker. The role will mount and decrypt the SD card onto the workstation.

6. __ws-docker-config__:

    The role install docker and make it running without sudo. Needed for code

7.  __ws-users-config__:

    Install my dotfiles from its repo, ssh key

1.  __ws-onedrive-config__:

    Install onedrive from source and set up my user configuration to prepare the synchro

2.  __ws-azure-log__:

    Install the OMS agent to control the workstation update directly from azure. Not needed, but interesting to test.

3.  __ws-upgrade__:

    The role will perform the upgrade of the distribution, packages and trigger a notification in slack to let me know if a reboot is pending

4.  __ws-slack-notification__:

   The playbook will send a notification if it's a success or a failure. In case of failure, the error message is sent into the channel

## Prerequisites

* Operating system:
  * Fresh install of Ubuntu 19.10 (tested on Kubuntu/ubuntu 19.10)
  * ansible installed
  * git installed
  * python3 installed
  * python installed
* Azure:
  * [Azure account](https://azure.microsoft.com/en-us/free/)
    * [Azure subscription](https://docs.microsoft.com/en-us/azure/cost-management-billing/manage/create-subscription)
      * [Azure key vault](https://docs.microsoft.com/en-us/azure-stack/user/azure-stack-key-vault-manage-portal?view=azs-2002) with the following secret
        * Bitlocker recovery password
        * Slack token
        * Ansible account ssh private key
        * Main user account ssh private key
        * Azure log workspace key

        ![secret](/doc/images/azure_secret.png)

      * [Azure log workspace](https://docs.microsoft.com/en-us/azure/azure-monitor/learn/quick-create-workspace) for the update
      * [Azure automation](https://docs.microsoft.com/en-us/azure/automation/automation-create-standalone-account) account to control the update
      * [Azure principal name](https://docs.microsoft.com/en-us/azure/active-directory/develop/howto-create-service-principal-portal) allowed to read the azure key vault

        ![app-registration](/doc/images/azure_app_registration.png)

* Slack:
  * [Slack account](https://slack.com/get-started#/)
    * [Slack webhook](https://api.slack.com/messaging/webhooks) token on a specific channel
* dotfiles
  * A [git repo](https://gitlab.com/tle06/dotfiles) with your dotfiles config
* [Env variable](#set-the-env-variable) for Azure account

## First run

### Install packages

```cmd
sudo apt update
sudo apt install git ansible python3 python nano -y
```

### Set environment variables

Into the file ```/etc/environment``` add the following environment variable.

* __AZ_WS_USER__: The user-principal account that has read access on your key vault
* __AZ_WS_USER_PWD__: The password of the user principal account
* __AZ_TENANT_ID__: your Azure tenant ID

You shoudl have something like that:

``` txt
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
AZ_WS_USER=75931262-8703-3118-7381-832704680749
AZ_WS_USER_PWD=9doUR&kWjbc09RnvX6Bx@XUjj*guyfkA
AZ_TENANT_ID=38d4c435-2034-3487-ert1-6614r5091e39
```

### Run ansible

The command will pull the repo and trigger ansible as sudo

```cmd
sudo ansible-pull -U https://gitlab.com/tle06/ansible-workstation-deployment.git
```

## Configuration

The configuration is split into 2 files.

### Main configuration

The file ```/group_vars/all.yml``` contain the configuration for

* User configuration
* Slack secret configuration
* Bitlocker_secret
* Git repo URL for ansible
* Azure log secret configuration
* Softwares configuration
  * apt packages
  * snap packages
  * snap with channel
  * deb packages
  * external packages
  * pip packages

### Notification configuration

The file ```/roles/ws-slack-notification/vars``` contain the configuration for:

* ```failure.yml```: general message when ansible fail

  ![fail](/doc/images/slack_fail.png)

* ```sucess.yml```: general message when ansible success

  ![fail](/doc/images/slack_success.png)

* ```onedrive.yml```: message if onedrive package is updated

  ![fail](/doc/images/slack_onedrive.png)

* ```upgrade.yml```: message to notify that a reboot is pending

  ![fail](/doc/images/slack_upgrade.png)

## Azure update management

Inside your automation account under update management, you should see your workstation after 15min. Don't forget to enable your device under ```Manage Machines``` menu. You will see the device compliance after 15 min.

![update-mgm](/doc/images/azure_update_mgm.png)
